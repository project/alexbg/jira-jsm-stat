CREATE DATABASE jsmstat;

CREATE ROLE jsmstat;
GRANT SELECT ON jsmstat.* TO jsmstat;
GRANT INSERT ON jsmstat.* TO jsmstat;
GRANT CREATE TABLE ON jsmstat.* TO jsmstat;

CREATE USER jsmstat IDENTIFIED WITH sha256_password BY 'pwd4jsmstat'
DEFAULT ROLE jsmstat
DEFAULT DATABASE jsmstat
GRANTEES NONE;

USE jsmstat;
