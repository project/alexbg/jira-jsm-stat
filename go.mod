module jsmstat

go 1.19

require (
	github.com/ClickHouse/clickhouse-go v1.5.4
	github.com/jmoiron/sqlx v1.3.5
	github.com/stretchr/testify v1.8.2
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/cloudflare/golz4 v0.0.0-20150217214814-ef862a3cdc58 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fatih/structs v1.1.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.5.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
