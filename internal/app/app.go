package app

import (
	"jsmstat/internal/config"
	"jsmstat/internal/service"
	"jsmstat/pkg/flags"
	"jsmstat/pkg/logger"
	"jsmstat/pkg/os"
)

type App struct {
	osp os.DefaultOsProvider
	log logger.ILogger
	cfg *config.Config
}

func Run() {
	app := App{}
	app.Run()
}

func (app *App) Run() {
	osp := &os.DefaultOsProvider{}
	log := &logger.DefaultLogger{}
	flags := flags.NewDefaultFlagProvider(log)

	cfg, err := config.NewConfig(osp, flags, log)
	app.cfg = cfg

	if err != nil {
		log.Fatal(err.Error())
		return
	}

	service := service.Init(cfg)
	service.ListenAndServe()
}
