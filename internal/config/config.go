package config

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"jsmstat/pkg/db"
	"jsmstat/pkg/flags"
	"jsmstat/pkg/logger"
	"jsmstat/pkg/os"

	"gopkg.in/yaml.v3"
)

const (
	Default_AppName            = "Jira-Stat"
	Default_Port               = 8080
	Default_DBName             = "jsmstat"
	Default_MonitoringDuration = 5 * time.Minute

	Flag_AppName             = "appname"
	Flag_Port                = "port"
	Flag_BaseUrl             = "baseurl"
	Flag_ApiToken            = "apitoken"
	Flag_DBHost              = "dbhost" // IP e.g. 127.0.0.1
	Flag_DBPort              = "dbport" // port number e.g. 9000
	Flag_DBName              = "dbname"
	Flag_DBUser              = "dbuser"
	Flag_DBPwd               = "dbpwd"
	Flag_MonitoringDuration  = "mduration"  // duration e.g. 5m0s
	Flag_MonitoringStartDate = "mstartdate" // date e.g. 2022-05-09
	Flag_MonitoringConfig    = "mcfg"       // ./monitoring.yaml

	Flags_Usage = `usage: %s
	Run Jira Stat Service
	
	Options:
	`

	Env_Prefix               = "JIRASTAT_"
	Env_AppName              = Env_Prefix + "APPNAME"
	Env_Port                 = Env_Prefix + "PORT"
	Env_BaseUrl              = Env_Prefix + "BASEURL"
	Env_ApiToken             = Env_Prefix + "APITOKEN"
	Env_ApiTokenFile         = Env_Prefix + "APITOKENFILE"
	Env_DBHost               = Env_Prefix + "DBHOST"
	Env_DBPort               = Env_Prefix + "DBPORT"
	Env_DBName               = Env_Prefix + "DBNAME"
	Env_DBUser               = Env_Prefix + "DBUSER"
	Env_DBPwd                = Env_Prefix + "DBPWD"
	Env_DBCredentialsFile    = Env_Prefix + "DBCREDENTIALSFILE"
	Env_MonitoringDuration   = Env_Prefix + "MDURATION"
	Env_MonitoringStartDate  = Env_Prefix + "MSTARTDATE"
	Env_MonitoringConfigFile = Env_Prefix + "MCONFIG"
)

type Config struct {
	flags     flags.IFlagsProvider
	OsPovider os.IOsProvider
	Logger    logger.ILogger

	AppName    string
	Port       int
	BaseUrl    string
	ApiToken   string
	DBHost     string
	DBPort     int
	DBName     string
	DBUser     string
	DBPassword string

	Monitoring YamlMonitoringConfig
}

type YamlDateTime struct {
	time.Time
}

type YamlMonitoringConfig struct {
	Duration  time.Duration `yaml:"duration"`
	StartDate YamlDateTime  `yaml:"startDate"`

	Products map[string]YamlProductConfig `yaml:"products"`
}

type YamlProductConfig struct {
	parentConfig *Config
	name         string

	JiraProjectID string        `yaml:"projectID"`
	LabelNames    []string      `yaml:"labels"`
	Duration      time.Duration `yaml:"duration"`
	StartDate     YamlDateTime  `yaml:"startDate"`
}

const DateTimeLayout = "2006-01-02"

func (ct *YamlDateTime) UnmarshalYAML(unmarshal func(interface{}) error) (err error) {
	var buffer string
	err = unmarshal(&buffer)
	if err != nil {
		return err
	}

	ct.Time, err = time.Parse(DateTimeLayout, strings.TrimSpace(buffer))
	return err
}

type DBCredentialsYaml struct {
	User     string `yaml:"dbuser"`
	Password string `yaml:"password"`
}

func NewConfig(osp os.IOsProvider, flags flags.IFlagsProvider, logger logger.ILogger) (*Config, error) {
	if osp == nil {
		return nil, errors.New("IOSProvider required")
	}
	if flags == nil {
		return nil, errors.New("IFlagsProvider required")
	}
	if logger == nil {
		return nil, errors.New("Logger required")
	}

	cfg := &Config{
		flags:     flags,
		OsPovider: osp,
		Logger:    logger,
	}

	cfg.InitEnvOrDefaults()
	cfg.InitWithFlags()
	err := cfg.FailIfInvalid()
	return cfg, err
}

func (cfg *Config) InitEnvOrDefaults() {
	cfg.AppName = getAppNameEnvOrDefault(cfg.OsPovider)
	cfg.BaseUrl = getBaseUrlEnvOrDefault(cfg.OsPovider)
	cfg.Port = getPortEnvOrDefault(cfg.OsPovider, cfg.Logger)
	cfg.ApiToken = getAPITokenEnvOrDefault(cfg.OsPovider, cfg.Logger)
	cfg.DBHost = getDBHostEnvOrDefault(cfg.OsPovider)
	cfg.DBPort = getDBPortEnvOrDefault(cfg.OsPovider, cfg.Logger)
	cfg.DBName = getDBNameEnvOrDefault(cfg.OsPovider)
	cfg.DBUser = getDBUserEnvOrDefault(cfg.OsPovider, cfg.Logger)
	cfg.DBPassword = getDBPasswordEnvOrDefault(cfg.OsPovider, cfg.Logger)

	cfg.Monitoring.Duration = getMonitoringDuration(cfg.OsPovider, cfg.Logger)
	cfg.Monitoring.StartDate = getMonitoringStartDate(cfg.OsPovider, cfg.Logger)
	cfg.loadMonitoringConfig(&cfg.Monitoring, cfg.OsPovider, cfg.Logger)
}

func (cfg *Config) InitWithFlags() {
	flag := cfg.flags

	flag.StringVar(&cfg.AppName, Flag_AppName, cfg.AppName, "Application Name")
	flag.IntVar(&cfg.Port, Flag_Port, cfg.Port, "Service port")
	flag.StringVar(&cfg.BaseUrl, Flag_BaseUrl, cfg.BaseUrl, "Jira API Base URL")
	flag.StringVar(&cfg.ApiToken, Flag_ApiToken, cfg.ApiToken, "Jira API Authorization Token")
	flag.StringVar(&cfg.DBHost, Flag_DBHost, cfg.DBHost, "Database host address, e.g. '127.0.0.1'")
	flag.IntVar(&cfg.DBPort, Flag_DBPort, cfg.DBPort, "Database host address, e.g. '9000'")
	flag.StringVar(&cfg.DBName, Flag_DBName, cfg.DBName, "Database name")
	flag.StringVar(&cfg.DBUser, Flag_DBUser, cfg.DBUser, "Database user name")
	flag.StringVar(&cfg.DBPassword, Flag_DBPwd, cfg.DBPassword, "Database user password")
	flag.DurationVar(&cfg.Monitoring.Duration, Flag_MonitoringDuration, cfg.Monitoring.Duration, "Jira monitoring interval duration")
	flag.DateVar(&cfg.Monitoring.StartDate.Time, Flag_MonitoringStartDate, cfg.Monitoring.StartDate.Time, "Jira monitoring start date")

	flag.SetUsage(func() {
		fmt.Fprintf(flag.GetCommandLine().Output(), Flags_Usage, cfg.OsPovider.GetArgs()[0])
		flag.PrintDefaults()
	})
	flag.Parse()
}

func (cfg *Config) FailIfInvalid() error {
	if cfg.AppName == "" {
		return errors.New(wrapValidation("App Name is required"))
	}

	if cfg.BaseUrl == "" {
		return errors.New(wrapValidation("Jira API Base URL is required"))
	}

	if cfg.ApiToken == "" {
		return errors.New(wrapValidation("Jira API Authorization Token is required"))
	}

	if cfg.DBHost == "" {
		return errors.New(wrapValidation("Database host address is required"))
	}

	if cfg.DBPort == 0 {
		return errors.New(wrapValidation("Database port is required"))
	}

	return nil
}

func getAppNameEnvOrDefault(osp os.IOsProvider) string {
	str := osp.GetEnv(Env_AppName)
	if str == "" {
		return Default_AppName
	}

	return str
}

func getBaseUrlEnvOrDefault(osp os.IOsProvider) string {
	return osp.GetEnv(Env_BaseUrl)
}

func getPortEnvOrDefault(osp os.IOsProvider, logger logger.ILogger) int {
	str := osp.GetEnv(Env_Port)
	if str == "" {
		return Default_Port
	}

	port, err := strconv.Atoi(str)
	if err != nil {
		logger.Warning(wrapCfg(Env_Port, str, fmt.Sprintf("So the default value is used. Port = %v", Default_Port)))
		return Default_Port
	}

	return port
}

func getAPITokenEnvOrDefault(osp os.IOsProvider, logger logger.ILogger) string {
	apiTokenStr := osp.GetEnv(Env_ApiToken)
	if apiTokenStr != "" {
		return apiTokenStr
	}

	apiTokenFilePath := osp.GetEnv(Env_ApiTokenFile)
	if apiTokenFilePath == "" {
		return ""
	}

	_, err := osp.Stat(apiTokenFilePath)
	if err != nil {
		logger.Error(wrapCfg(Env_ApiTokenFile, apiTokenFilePath, err.Error()))
		return ""
	}

	apiToken, err := osp.ReadFile(apiTokenFilePath)
	if err != nil {
		logger.Error(wrapCfg(Env_ApiTokenFile, apiTokenFilePath, err.Error()))
	}
	return string(apiToken)
}

func getDBHostEnvOrDefault(osp os.IOsProvider) string {
	return osp.GetEnv(Env_DBHost)
}

func getDBPortEnvOrDefault(osp os.IOsProvider, logger logger.ILogger) int {
	str := osp.GetEnv(Env_DBPort)
	port, err := strconv.Atoi(str)
	if err != nil {
		logger.Error(wrapCfg(Env_DBPort, str, ""))
		return 0
	}
	return port
}

func getDBNameEnvOrDefault(osp os.IOsProvider) string {
	str := osp.GetEnv(Env_DBName)
	if str == "" {
		return Default_DBName
	}

	return str
}

func getDBUserEnvOrDefault(osp os.IOsProvider, logger logger.ILogger) string {
	dbUser := osp.GetEnv(Env_DBUser)
	if dbUser != "" {
		return dbUser
	}

	dbCreds := getDBCredentialsYaml(osp, logger)
	return dbCreds.User
}

func getDBPasswordEnvOrDefault(osp os.IOsProvider, logger logger.ILogger) string {
	dbPwd := osp.GetEnv(Env_DBPwd)
	if dbPwd != "" {
		return dbPwd
	}

	dbCreds := getDBCredentialsYaml(osp, logger)
	return dbCreds.Password
}

func getDBCredentialsYaml(osp os.IOsProvider, logger logger.ILogger) DBCredentialsYaml {
	dbCredentialsFilePath := osp.GetEnv(Env_DBCredentialsFile)
	if dbCredentialsFilePath == "" {
		return DBCredentialsYaml{}
	}

	_, err := osp.Stat(dbCredentialsFilePath)
	if err != nil {
		logger.Error(wrapCfg(Env_DBCredentialsFile, dbCredentialsFilePath, err.Error()))
		return DBCredentialsYaml{}
	}

	fileData, err := osp.ReadFile(dbCredentialsFilePath)
	if err != nil {
		logger.Error(wrapCfg(Env_DBCredentialsFile, dbCredentialsFilePath, err.Error()))
		return DBCredentialsYaml{}
	}

	var dbCreds DBCredentialsYaml
	err = yaml.Unmarshal(fileData, &dbCreds)
	if err != nil {
		logger.Error(wrapCfg(Env_DBCredentialsFile, dbCredentialsFilePath, err.Error()))
		return DBCredentialsYaml{}
	}
	return dbCreds
}

func getMonitoringDuration(osp os.IOsProvider, logger logger.ILogger) time.Duration {
	str := osp.GetEnv(Env_MonitoringDuration)
	if str == "" {
		return Default_MonitoringDuration
	}

	duration, err := time.ParseDuration(str)
	if err != nil {
		logger.Warning(wrapCfg(Env_MonitoringDuration, str,
			fmt.Sprintf("So the default value is used. Duration = %v", Default_MonitoringDuration)))
		return Default_MonitoringDuration
	}

	return duration
}

func getMonitoringStartDate(osp os.IOsProvider, logger logger.ILogger) YamlDateTime {
	defaultStartDate := time.Now().Add(-365 * 24 * time.Hour)

	str := osp.GetEnv(Env_MonitoringStartDate)
	if str == "" {
		return YamlDateTime{defaultStartDate}
	}

	startDate, err := time.Parse(flags.DateFormat, str)
	if err != nil {
		logger.Warning(wrapCfg(Env_MonitoringStartDate, str,
			fmt.Sprintf("So the default value is used. Duration = %v", defaultStartDate.Format(flags.DateFormat))))
		return YamlDateTime{defaultStartDate}
	}

	return YamlDateTime{startDate}
}

func (cfg *Config) loadMonitoringConfig(mcfg *YamlMonitoringConfig, osp os.IOsProvider, logger logger.ILogger) {
	mcfgFilePath := osp.GetEnv(Env_MonitoringConfigFile)
	if mcfgFilePath == "" {
		return
	}

	_, err := osp.Stat(mcfgFilePath)
	if err != nil {
		logger.Error(wrapCfg(Env_MonitoringConfigFile, mcfgFilePath, err.Error()))
		return
	}

	fileData, err := osp.ReadFile(mcfgFilePath)
	if err != nil {
		logger.Error(wrapCfg(Env_MonitoringConfigFile, mcfgFilePath, err.Error()))
		return
	}

	err = yaml.Unmarshal(fileData, mcfg)
	if err != nil {
		logger.Error(wrapCfg(Env_MonitoringConfigFile, mcfgFilePath, err.Error()))
		return
	}

	for name, _ := range mcfg.Products {
		p := mcfg.Products[name]
		p.parentConfig = cfg
		p.name = name
		mcfg.Products[name] = p
	}
}

func wrapCfg(envVar string, envValue string, message string) string {
	return fmt.Sprintf("Config: %v = %v is invalid. %v", envVar, envValue, message)
}
func wrapValidation(message string) string {
	return fmt.Sprintf("Config Validation Error: %v", message)
}

// IDBConfig interface implementation
func (cfg *Config) GetLogger() logger.ILogger {
	return cfg.Logger
}
func (cfg *Config) GetDBConfig() db.IDBConfig {
	return cfg
}
func (cfg *Config) GetDBHost() string {
	return cfg.DBHost
}
func (cfg *Config) GetDBPort() int {
	return cfg.DBPort
}
func (cfg *Config) GetDBName() string {
	return cfg.DBName
}
func (cfg *Config) GetDBUser() string {
	return cfg.DBUser
}
func (cfg *Config) GetDBPassword() string {
	return cfg.DBPassword
}

// IMonitorConfig & IProduct  interface implementation
func (product *YamlProductConfig) GetLogger() logger.ILogger {
	return product.parentConfig.Logger
}
func (product *YamlProductConfig) ProjectID() string {
	return product.JiraProjectID
}
func (product *YamlProductConfig) Labels() []string {
	return product.LabelNames
}
func (product *YamlProductConfig) GetProductName() string {
	return product.name
}
func (product *YamlProductConfig) GetMonitoringDuration() time.Duration {
	if product.Duration == 0 {
		return product.parentConfig.Monitoring.Duration
	}

	return product.Duration
}
func (product *YamlProductConfig) GetMonitoringStartDate() time.Time {
	if product.StartDate.Time.IsZero() {
		return product.parentConfig.Monitoring.StartDate.Time
	}

	return product.StartDate.Time
}

// ITaskHistoryRepoConfig interface implementation
func (product *YamlProductConfig) GetTaskHistoryRepoName() string {
	return product.GetProductName() + "_history"
}

// IJiraClientConfig interface implementation
func (cfg *Config) GetBaseUrl() string {
	return cfg.BaseUrl
}
func (cfg *Config) GetApiToken() string {
	return cfg.ApiToken
}
