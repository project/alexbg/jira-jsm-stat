package config

import (
	"errors"
	"io/ioutil"
	"testing"
	"time"

	"jsmstat/internal/config"
	"jsmstat/pkg/flags"
	"jsmstat/pkg/logger"
	"jsmstat/pkg/os"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func NewTestConfig(osp *os.MockOsProvider, fl *flags.MockFlagsProvider) (*config.Config, error) {
	return config.NewConfig(osp, fl, &logger.DefaultLogger{})
}
func expectedDefaultMonitoringStartDate() string {
	return time.Now().Add(-365 * 24 * time.Hour).Format(flags.DateFormat)
}
func assertProduct(t *testing.T, product *config.YamlProductConfig, name string, projectID string, labels []string, duration string, startDate string) {
	assert.Equal(t, name, product.GetProductName())
	assert.Equal(t, projectID, product.ProjectID())
	assert.Equal(t, labels, product.Labels())
	assert.Equal(t, duration, product.Duration.String())
	assert.Equal(t, startDate, product.StartDate.Format(flags.DateFormat))
}
func getTestData(t *testing.T, filePath string) string {
	bytes, err := ioutil.ReadFile(filePath)
	assert.NoError(t, err)
	return string(bytes)
}
func TestConfigDefaults(t *testing.T) {
	var osp os.MockOsProvider
	osp.On("GetEnv", mock.Anything).Return("")

	cfg, _ := NewTestConfig(&osp, flags.NewMockFlagsProvider())

	assert.Equal(t, config.Default_AppName, cfg.AppName)
	assert.Equal(t, config.Default_DBName, cfg.DBName)
	assert.Equal(t, config.Default_Port, cfg.Port)
	assert.Equal(t, config.Default_MonitoringDuration, cfg.Monitoring.Duration)

	expectedDefaultDate := expectedDefaultMonitoringStartDate()
	actualDate := cfg.Monitoring.StartDate.Format(flags.DateFormat)
	assert.Equal(t, expectedDefaultDate, actualDate)

	assert.Empty(t, cfg.ApiToken)
	assert.Empty(t, cfg.BaseUrl)
	assert.Empty(t, cfg.DBHost)
	assert.Empty(t, cfg.DBPort)
	assert.Empty(t, cfg.DBUser)
	assert.Empty(t, cfg.DBPassword)
	assert.Empty(t, cfg.DBPassword)
}

func TestConfigFromEnvs(t *testing.T) {
	var osp os.MockOsProvider
	osp.On("GetEnv", config.Env_AppName).Return("TestApp").
		On("GetEnv", config.Env_Port).Return("123456").
		On("GetEnv", config.Env_BaseUrl).Return("test.base/url").
		On("GetEnv", config.Env_ApiToken).Return("TestAPIToken").
		On("GetEnv", config.Env_DBHost).Return("TestDBHost").
		On("GetEnv", config.Env_DBPort).Return("9000").
		On("GetEnv", config.Env_DBName).Return("TestDBName").
		On("GetEnv", config.Env_DBUser).Return("TestDBUser").
		On("GetEnv", config.Env_DBPwd).Return("TestDBPwd").
		On("GetEnv", config.Env_MonitoringDuration).Return("5m30s").
		On("GetEnv", config.Env_MonitoringStartDate).Return("2022-05-09").
		On("GetEnv", config.Env_MonitoringConfigFile).Return("")

	cfg, _ := NewTestConfig(&osp, flags.NewMockFlagsProvider())

	assert.Equal(t, "TestApp", cfg.AppName)
	assert.Equal(t, 123456, cfg.Port)
	assert.Equal(t, "test.base/url", cfg.BaseUrl)
	assert.Equal(t, "TestAPIToken", cfg.ApiToken)
	assert.Equal(t, "TestDBHost", cfg.DBHost)
	assert.Equal(t, 9000, cfg.DBPort)
	assert.Equal(t, "TestDBName", cfg.DBName)
	assert.Equal(t, "TestDBUser", cfg.DBUser)
	assert.Equal(t, "TestDBPwd", cfg.DBPassword)
	assert.Equal(t, "5m30s", cfg.Monitoring.Duration.String())
	assert.Equal(t, "2022-05-09", cfg.Monitoring.StartDate.Format(flags.DateFormat))
	assert.Equal(t, 0, len(cfg.Monitoring.Products))
}

func TestConfigApiTokenFromFile_NoTokenInEnv_NoFile(t *testing.T) {
	var osp os.MockOsProvider
	const testFilePath = "/test/token/file"
	osp.On("GetEnv", config.Env_ApiToken).Return("").
		On("GetEnv", config.Env_ApiTokenFile).Return(testFilePath).
		On("GetEnv", mock.Anything).Return("").
		On("Stat", testFilePath).Return(nil, errors.New("no file"))

	cfg, _ := NewTestConfig(&osp, flags.NewMockFlagsProvider())

	assert.Empty(t, cfg.ApiToken)
}

func TestConfigApiTokenFromFile_NoTokenInEnv_ReadFileError(t *testing.T) {
	var osp os.MockOsProvider
	const testFilePath = "/test/token/file"
	osp.On("GetEnv", config.Env_ApiToken).Return("").
		On("GetEnv", config.Env_ApiTokenFile).Return(testFilePath).
		On("GetEnv", mock.Anything).Return("").
		On("Stat", testFilePath).Return(nil, nil).
		On("ReadFile", testFilePath).Return(nil, errors.New("read file error"))

	cfg, _ := NewTestConfig(&osp, flags.NewMockFlagsProvider())

	assert.Empty(t, cfg.ApiToken)
}

func TestConfigApiTokenFromFile_NoTokenInEnv_ReadFile(t *testing.T) {
	var osp os.MockOsProvider
	const testFilePath = "/test/token/file"
	osp.On("GetEnv", config.Env_ApiToken).Return("").
		On("GetEnv", config.Env_ApiTokenFile).Return(testFilePath).
		On("GetEnv", mock.Anything).Return("").
		On("Stat", testFilePath).Return(nil, nil).
		On("ReadFile", testFilePath).Return("TestAPIToken", nil)

	cfg, _ := NewTestConfig(&osp, flags.NewMockFlagsProvider())

	assert.Equal(t, "TestAPIToken", cfg.ApiToken)
}

func TestConfigApiTokenFromFile_GetFromEnvsIfExist(t *testing.T) {
	var osp os.MockOsProvider
	const testFilePath = "/test/token/file"
	osp.On("GetEnv", config.Env_ApiToken).Return("TestAPITokenEnv").
		On("GetEnv", config.Env_ApiTokenFile).Return(testFilePath).
		On("GetEnv", mock.Anything).Return("").
		On("Stat", testFilePath).Return(nil, nil).
		On("ReadFile", testFilePath).Return("TestAPIToken", errors.New("read file error"))

	cfg, _ := NewTestConfig(&osp, flags.NewMockFlagsProvider())

	assert.Equal(t, "TestAPITokenEnv", cfg.ApiToken)
}

func TestConfigDBCredsFromFile_NoCredsInEnv_NoFile(t *testing.T) {
	var osp os.MockOsProvider
	const testFilePath = "/test/dbcreds/file"
	osp.On("GetEnv", config.Env_DBUser).Return("").
		On("GetEnv", config.Env_DBPwd).Return("").
		On("GetEnv", config.Env_DBCredentialsFile).Return(testFilePath).
		On("GetEnv", mock.Anything).Return("").
		On("Stat", testFilePath).Return(nil, errors.New("no file"))

	cfg, _ := NewTestConfig(&osp, flags.NewMockFlagsProvider())

	assert.Empty(t, cfg.DBUser)
	assert.Empty(t, cfg.DBPassword)
}

func TestConfigDBCredsFromFile_NoCredsInEnv_ReadFileError(t *testing.T) {
	var osp os.MockOsProvider
	const testFilePath = "/test/dbcreds/file"
	osp.On("GetEnv", config.Env_DBUser).Return("").
		On("GetEnv", config.Env_DBPwd).Return("").
		On("GetEnv", config.Env_DBCredentialsFile).Return(testFilePath).
		On("GetEnv", mock.Anything).Return("").
		On("Stat", testFilePath).Return(nil, nil).
		On("ReadFile", testFilePath).Return(nil, errors.New("read file error"))

	cfg, _ := NewTestConfig(&osp, flags.NewMockFlagsProvider())

	assert.Empty(t, cfg.DBUser)
	assert.Empty(t, cfg.DBPassword)
}

func TestConfigDBCredsFromFile_NoCredsInEnv_ReadFile(t *testing.T) {
	var osp os.MockOsProvider
	const testFilePath = "/test/dbcreds/file"
	osp.On("GetEnv", config.Env_DBUser).Return("").
		On("GetEnv", config.Env_DBPwd).Return("").
		On("GetEnv", config.Env_DBCredentialsFile).Return(testFilePath).
		On("GetEnv", mock.Anything).Return("").
		On("Stat", testFilePath).Return(nil, nil).
		On("ReadFile", testFilePath).Return("dbuser: YamlDBUser\npassword: YamlDBPwd", nil)

	cfg, _ := NewTestConfig(&osp, flags.NewMockFlagsProvider())

	assert.Equal(t, "YamlDBUser", cfg.DBUser)
	assert.Equal(t, "YamlDBPwd", cfg.DBPassword)
}

func TestConfigDBCredsFromFile_GetFromEnvsIfExist(t *testing.T) {
	var osp os.MockOsProvider
	const testFilePath = "/test/dbcreds/file"
	osp.On("GetEnv", config.Env_DBUser).Return("EnvDBUser").
		On("GetEnv", config.Env_DBPwd).Return("EnvDBPwd").
		On("GetEnv", config.Env_DBCredentialsFile).Return(testFilePath).
		On("GetEnv", mock.Anything).Return("").
		On("Stat", testFilePath).Return(nil, nil).
		On("ReadFile", testFilePath).Return("dbuser: YamlDBUser\npassword: YamlDBPwd", nil)

	cfg, _ := NewTestConfig(&osp, flags.NewMockFlagsProvider())

	assert.Equal(t, "EnvDBUser", cfg.DBUser)
	assert.Equal(t, "EnvDBPwd", cfg.DBPassword)
}

func TestConfigMonitoringFromFile_NothingInEnv_NoFile(t *testing.T) {
	var osp os.MockOsProvider
	const testFilePath = "/test/monitoring/config"
	osp.On("GetEnv", config.Env_MonitoringDuration).Return("").
		On("GetEnv", config.Env_MonitoringStartDate).Return("").
		On("GetEnv", config.Env_MonitoringConfigFile).Return(testFilePath).
		On("GetEnv", mock.Anything).Return("").
		On("Stat", testFilePath).Return(nil, errors.New("no file"))

	cfg, _ := NewTestConfig(&osp, flags.NewMockFlagsProvider())

	assert.Equal(t, config.Default_MonitoringDuration, cfg.Monitoring.Duration)

	expectedDefaultDate := expectedDefaultMonitoringStartDate()
	actualDate := cfg.Monitoring.StartDate.Format(flags.DateFormat)
	assert.Equal(t, expectedDefaultDate, actualDate)
	assert.Equal(t, 0, len(cfg.Monitoring.Products))
}

func TestConfigMonitoringFromFile_NothingInEnv_ReadFileError(t *testing.T) {
	var osp os.MockOsProvider
	const testFilePath = "/test/monitoring/config"
	osp.On("GetEnv", config.Env_MonitoringDuration).Return("").
		On("GetEnv", config.Env_MonitoringStartDate).Return("").
		On("GetEnv", config.Env_MonitoringConfigFile).Return(testFilePath).
		On("GetEnv", mock.Anything).Return("").
		On("Stat", testFilePath).Return(nil, nil).
		On("ReadFile", testFilePath).Return(nil, errors.New("read file error"))

	cfg, _ := NewTestConfig(&osp, flags.NewMockFlagsProvider())

	assert.Equal(t, config.Default_MonitoringDuration, cfg.Monitoring.Duration)

	expectedDefaultDate := expectedDefaultMonitoringStartDate()
	actualDate := cfg.Monitoring.StartDate.Format(flags.DateFormat)
	assert.Equal(t, expectedDefaultDate, actualDate)
	assert.Equal(t, 0, len(cfg.Monitoring.Products))
}

func TestConfigMonitoringFromFile_NothingInEnv_ReadFile(t *testing.T) {
	var osp os.MockOsProvider
	const testFilePath = "/test/monitoring/config"
	osp.On("GetEnv", config.Env_MonitoringDuration).Return("").
		On("GetEnv", config.Env_MonitoringStartDate).Return("").
		On("GetEnv", config.Env_MonitoringConfigFile).Return(testFilePath).
		On("GetEnv", mock.Anything).Return("").
		On("Stat", testFilePath).Return(nil, nil).
		On("ReadFile", testFilePath).Return(getTestData(t, "monitoring_test.yaml"), nil)

	cfg, _ := NewTestConfig(&osp, flags.NewMockFlagsProvider())

	assert.Equal(t, "20s", cfg.Monitoring.Duration.String())
	assert.Equal(t, "2021-01-01", cfg.Monitoring.StartDate.Format(flags.DateFormat))
	assert.Equal(t, 2, len(cfg.Monitoring.Products))

	product := cfg.Monitoring.Products["operator"]
	assertProduct(t, &product,
		"operator", "EDO", []string{"Оператор.ЭДО"},
		"1m30s", "2022-01-01",
	)

	product = cfg.Monitoring.Products["operatorTP"]
	assertProduct(t, &product,
		"operatorTP", "OEDOTP", nil,
		"2m0s", "2022-02-01",
	)
}

func TestConfigMonitoringFromFile_GetFromEnvsIfExist(t *testing.T) {
	var osp os.MockOsProvider
	const testFilePath = "/test/monitoring/config"
	osp.On("GetEnv", config.Env_MonitoringDuration).Return("5m30s").
		On("GetEnv", config.Env_MonitoringStartDate).Return("2021-01-01").
		On("GetEnv", config.Env_MonitoringConfigFile).Return(testFilePath).
		On("GetEnv", mock.Anything).Return("").
		On("Stat", testFilePath).Return(nil, nil).
		On("ReadFile", testFilePath).Return(getTestData(t, "monitoring_no_root_settings_test.yaml"), nil)

	cfg, _ := NewTestConfig(&osp, flags.NewMockFlagsProvider())

	assert.Equal(t, "5m30s", cfg.Monitoring.Duration.String())
	assert.Equal(t, "2021-01-01", cfg.Monitoring.StartDate.Format(flags.DateFormat))
	assert.Equal(t, 2, len(cfg.Monitoring.Products))
}

func TestConfig_FailIfInvalid_NoError(t *testing.T) {
	cfg := config.Config{
		AppName:  "AppName",
		BaseUrl:  "BaseUrl",
		ApiToken: "APIToken",
		DBHost:   "DBHost",
		DBPort:   9000,
	}

	assert.NoError(t, cfg.FailIfInvalid())
}
func TestConfig_FailIfInvalid_AppName(t *testing.T) {
	cfg := config.Config{
		AppName:  "",
		BaseUrl:  "BaseUrl",
		ApiToken: "APIToken",
		DBHost:   "DBHost",
		DBPort:   9000,
	}

	assert.Error(t, cfg.FailIfInvalid())
}
func TestConfig_FailIfInvalid_BaseUrl(t *testing.T) {
	cfg := config.Config{
		AppName:  "AppName",
		BaseUrl:  "",
		ApiToken: "APIToken",
		DBHost:   "DBHost",
		DBPort:   9000,
	}

	assert.Error(t, cfg.FailIfInvalid())
}
func TestConfig_FailIfInvalid_APIToken(t *testing.T) {
	cfg := config.Config{
		AppName:  "AppName",
		BaseUrl:  "BaseUrl",
		ApiToken: "",
		DBHost:   "DBHost",
		DBPort:   9000,
	}

	assert.Error(t, cfg.FailIfInvalid())
}
func TestConfig_FailIfInvalid_DBHost(t *testing.T) {
	cfg := config.Config{
		AppName:  "AppName",
		BaseUrl:  "BaseUrl",
		ApiToken: "APIToken",
		DBHost:   "",
		DBPort:   9000,
	}

	assert.Error(t, cfg.FailIfInvalid())
}
func TestConfig_FailIfInvalid_DBPort(t *testing.T) {
	cfg := config.Config{
		AppName:  "AppName",
		BaseUrl:  "BaseUrl",
		ApiToken: "APIToken",
		DBHost:   "DBHost",
		DBPort:   0,
	}

	assert.Error(t, cfg.FailIfInvalid())
}
