package domain

import "time"

type Operation int

const (
	Created Operation = iota
	Updated
)

type TaskHistoryItem struct {
	TimeStamp   time.Time
	IssueID     string
	IssueTypeID int
	IssueType   string
	Author      string
	Operation   Operation
	StatusID    int
	StatusText  string
}
