package repositories

import (
	"jsmstat/pkg/db"
	"jsmstat/pkg/logger"
)

type IRepositoryConfig interface {
	GetLogger() logger.ILogger
}

type IRepository interface {
	InitStorage() error
}

type Repositories struct {
	taskHistoryRepos map[string]ITaskHistoryRepository
}

func (repos *Repositories) InitTaskHistoryRepo(cfg ITaskHistoryRepoConfig, db db.IDBHelper) (ITaskHistoryRepository, error) {
	if repos.taskHistoryRepos == nil {
		repos.taskHistoryRepos = make(map[string]ITaskHistoryRepository)
	}

	repo, err := NewTaskHistoryRepository(cfg, db)
	if err != nil {
		return nil, err
	}

	repos.taskHistoryRepos[cfg.GetTaskHistoryRepoName()] = repo
	return repo, nil
}
