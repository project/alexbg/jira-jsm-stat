package repositories

import (
	"database/sql"
	"fmt"
	"jsmstat/internal/domain"
	"jsmstat/pkg/db"
	"reflect"
	"time"
)

type ITaskHistoryRepoConfig interface {
	IRepositoryConfig
	GetTaskHistoryRepoName() string
}

type ITaskHistoryRepository interface {
	IRepository
	SaveItems(items []*domain.TaskHistoryItem) error
	GetLastTimeStamp(operation domain.Operation) (time.Time, error)
}

type taskHistoryRepository struct {
	cfg ITaskHistoryRepoConfig
	db  db.IDBHelper
}

func NewTaskHistoryRepository(cfg ITaskHistoryRepoConfig, db db.IDBHelper) (ITaskHistoryRepository, error) {
	repo := &taskHistoryRepository{
		cfg: cfg,
		db:  db,
	}
	err := repo.InitStorage()
	if err != nil {
		return nil, err
	}
	return repo, nil
}
func convertOperationToInt(op interface{}) interface{} {
	return int(op.(domain.Operation))
}
func (repo *taskHistoryRepository) InitStorage() error {
	repo.db.SetTypeConversion(reflect.TypeOf(domain.Created), convertOperationToInt)
	tableName := repo.cfg.GetTaskHistoryRepoName()
	tableExists, err := repo.db.IsTableExists(tableName)
	if err != nil {
		return err
	}
	if tableExists {
		return nil
	}
	err = repo.createTable()
	return err
}
func (repo *taskHistoryRepository) createTable() error {
	const sql = `
	CREATE TABLE %s
	(
    	TimeStamp DateTime('Europe/Moscow'), 
    	IssueID String, 
    	IssueTypeID int, 
    	Author String, 
    	Operation int, 
    	StatusID int, 
    	StatusText String
	)
	ENGINE = Log;`

	db, err := repo.db.Database()
	if err != nil {
		return err
	}

	tableName := repo.cfg.GetTaskHistoryRepoName()
	querySql := fmt.Sprintf(sql, tableName)
	err = db.ExecSql(querySql)
	if err != nil {
		return err
	}

	log := repo.cfg.GetLogger()
	log.Message("New table created in database: %v", tableName)
	return nil
}
func (repo *taskHistoryRepository) SaveItems(items []*domain.TaskHistoryItem) error {
	log := repo.cfg.GetLogger()
	tableName := repo.cfg.GetTaskHistoryRepoName()

	args := make([]interface{}, len(items))
	for i := range items {
		args[i] = items[i]
	}
	err := repo.db.InsertValues(tableName, args, []string{"IssueType"})
	if err != nil {
		return err
	}

	log.Message("Saved TaskHistoryItems: count=%v", len(items))
	return nil
}
func (repo *taskHistoryRepository) GetLastTimeStamp(operation domain.Operation) (time.Time, error) {
	db, err := repo.db.Database()
	if err != nil {
		return time.Time{}, err
	}

	tableName := repo.cfg.GetTaskHistoryRepoName()
	sqlRequest := fmt.Sprintf("select max(TimeStamp) from %s where Operation=%v", tableName, convertOperationToInt(operation))

	result := &time.Time{}
	process := func(rows *sql.Rows) (bool, error) {
		return false, rows.Scan(result)
	}

	if err = db.QueryRows(sqlRequest, process); err != nil {
		return time.Time{}, err
	}

	return *result, nil
}
