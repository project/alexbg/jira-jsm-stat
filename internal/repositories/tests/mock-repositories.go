package repositories

import (
	"time"

	"jsmstat/internal/domain"
	"jsmstat/pkg/logger"

	"github.com/stretchr/testify/mock"
)

const DateFormat = "2006-01-02"

type MockTaskHistoryRepoConfig struct {
	mock.Mock
}

func (m *MockTaskHistoryRepoConfig) GetLogger() logger.ILogger {
	return &logger.DefaultLogger{}
}
func (m *MockTaskHistoryRepoConfig) GetTaskHistoryRepoName() string {
	args := m.Called()
	return args.String(0)
}

type MockTaskHistoryRepository struct {
	mock.Mock
}

func (m *MockTaskHistoryRepository) InitStorage() error {
	args := m.Called()
	return args.Error(0)
}
func (m *MockTaskHistoryRepository) SaveItems(taskHistoryItem []*domain.TaskHistoryItem) error {
	args := m.Called(mock.Anything)
	return args.Error(0)
}
func (m *MockTaskHistoryRepository) GetLastTimeStamp(operation domain.Operation) (time.Time, error) {
	args := m.Called(operation)
	value := args.Get(0)
	err := args.Error(1)

	switch value.(type) {
	case nil:
		return time.Time{}, err
	case time.Time:
		return value.(time.Time), err
	case string:
		result, _ := time.Parse(DateFormat, value.(string))
		return result, err
	}

	return time.Time{}, err
}
