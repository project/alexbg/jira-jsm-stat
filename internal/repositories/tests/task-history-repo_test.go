package repositories

import (
	"errors"
	"jsmstat/internal/repositories"
	"jsmstat/pkg/db"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func Test_CreateNewTable_TableDoesntExist(t *testing.T) {
	testTableName := "test_repo"
	cfg := &MockTaskHistoryRepoConfig{}
	cfg.On("GetTaskHistoryRepoName").Return(testTableName)

	db := &db.MockDBHelper{}
	db.On("Database").Once().Return(nil).
		On("IsTableExists", testTableName).Once().Return(false, nil).
		On("ExecSql", mock.Anything).Once().Return(nil)

	repo, err := repositories.NewTaskHistoryRepository(cfg, db)
	assert.NoError(t, err)
	assert.NotNil(t, repo)
	db.AssertExpectations(t)
}
func Test_CreateNewTable_TableExists(t *testing.T) {
	testTableName := "test_repo"
	cfg := &MockTaskHistoryRepoConfig{}
	cfg.On("GetTaskHistoryRepoName").Return(testTableName)

	db := &db.MockDBHelper{}
	db.On("IsTableExists", testTableName).Once().Return(true, nil)

	repo, err := repositories.NewTaskHistoryRepository(cfg, db)
	assert.NoError(t, err)
	assert.NotNil(t, repo)
	db.AssertExpectations(t)
}
func Test_CreateNewTable_DBError1(t *testing.T) {
	testTableName := "test_repo"
	cfg := &MockTaskHistoryRepoConfig{}
	cfg.On("GetTaskHistoryRepoName").Return(testTableName)

	db := &db.MockDBHelper{}
	db.On("Database").Once().Return(errors.New("fail")).
		On("IsTableExists", testTableName).Once().Return(false, nil)

	repo, err := repositories.NewTaskHistoryRepository(cfg, db)
	assert.Error(t, err)
	assert.Nil(t, repo)
	db.AssertExpectations(t)
}
func Test_CreateNewTable_DBError2(t *testing.T) {
	testTableName := "test_repo"
	cfg := &MockTaskHistoryRepoConfig{}
	cfg.On("GetTaskHistoryRepoName").Return(testTableName)

	db := &db.MockDBHelper{}
	db.On("IsTableExists", testTableName).Once().Return(false, errors.New("fail"))

	repo, err := repositories.NewTaskHistoryRepository(cfg, db)
	assert.Error(t, err)
	assert.Nil(t, repo)
	db.AssertExpectations(t)
}
func Test_CreateNewTable_SQLError(t *testing.T) {
	testTableName := "test_repo"
	cfg := &MockTaskHistoryRepoConfig{}
	cfg.On("GetTaskHistoryRepoName").Return(testTableName)

	db := &db.MockDBHelper{}
	db.On("Database").Once().Return(nil).
		On("IsTableExists", testTableName).Once().Return(false, nil).
		On("ExecSql", mock.Anything).Once().Return(errors.New("fail"))

	repo, err := repositories.NewTaskHistoryRepository(cfg, db)
	assert.Error(t, err)
	assert.Nil(t, repo)
	db.AssertExpectations(t)
}
