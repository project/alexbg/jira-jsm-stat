package service

import (
	"encoding/json"
	"fmt"
	"net/http"

	"jsmstat/internal/config"
	"jsmstat/internal/repositories"
	"jsmstat/internal/service/jiraclient"
	"jsmstat/internal/service/monitor"
	"jsmstat/pkg/db"
	"jsmstat/pkg/db/clickhouse"
)

type JiraStatService struct {
	Config       *config.Config
	DB           db.IDBHelper
	Repositories repositories.Repositories
	Monitors     map[string]monitor.IMonitor
}

func (s *JiraStatService) InitMonitors() error {
	cfg := s.Config
	jiraclient := jiraclient.NewClientV2(cfg)

	result := make(map[string]monitor.IMonitor)
	for key, _ := range cfg.Monitoring.Products {
		productCfg := cfg.Monitoring.Products[key]
		repo, err := s.Repositories.InitTaskHistoryRepo(&productCfg, s.DB)
		if err != nil {
			return err
		}
		result[key] = monitor.InitMonitor(&productCfg, jiraclient, repo)
	}

	s.Monitors = result
	return nil
}
func (s *JiraStatService) StartMonitors() {
	for _, m := range s.Monitors {
		m.Start()
	}
}
func (s *JiraStatService) StopMonitors() {
	for _, m := range s.Monitors {
		m.Stop()
	}
}

func Init(cfg *config.Config) *JiraStatService {
	log := cfg.Logger

	log.Message("%v is Running on port=%v... \n", cfg.AppName, cfg.Port)
	log.Message(
		`Configuring with 
	Base URL: %v
	API Token: %v`,
		cfg.BaseUrl, maskApiToken(cfg.ApiToken),
	)

	service := &JiraStatService{
		Config: cfg,
		DB:     clickhouse.NewHelper(cfg),
	}

	err := service.InitMonitors()
	if err != nil {
		log.Fatal(err.Error())
		return nil
	}

	http.HandleFunc("/", service.rootHandler)
	return service
}

func (service *JiraStatService) ListenAndServe() {
	cfg := service.Config
	log := cfg.Logger

	service.StartMonitors()
	defer service.StopMonitors()

	err := http.ListenAndServe(fmt.Sprintf(":%v", cfg.Port), nil)
	if err != nil {
		log.Fatal(err.Error())
	}

}

func maskApiToken(token string) string {
	if token != "" {
		return "<has been set>"
	}
	return "<no token>"
}

type RootResponse struct {
	Message string
}

func (service *JiraStatService) rootHandler(w http.ResponseWriter, r *http.Request) {
	log := service.Config.Logger

	response := RootResponse{Message: "100"}
	data, err := json.Marshal(response)
	if err != nil {
		log.Fatal(fmt.Sprintf("%v in PANIC!!!", service.Config.AppName))
	}

	fmt.Fprintf(w, string(data))
}
