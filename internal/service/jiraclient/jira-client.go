package jiraclient

import (
	"time"

	"jsmstat/pkg/logger"
)

type IJiraClientConfig interface {
	GetLogger() logger.ILogger
	GetBaseUrl() string
	GetApiToken() string
}

type IProduct interface {
	ProjectID() string
	Labels() []string
}

type IIssue interface {
	IssueID() string
	Labels() []string
	Creator() IAuthor
	CreatedDate() time.Time
	Type() IIssueType
	Status() IIssueStatus
	HistoryItems() []IIssueHistoryItem
}

type IIssueStatus interface {
	StatusID() int
	DisplayName() string
}

type IIssueHistoryItem interface {
	Author() IAuthor
	CreatedDate() time.Time
	Field() string
	OldValue() string
	OldString() string
	NewValue() string
	NewString() string
}

type IAuthor interface {
	ShortName() string
	FullName() string
}

type IIssueType interface {
	TypeID() int
	DisplayName() string
}

type IJiraSearchResult interface {
	GetStartAt() int
	GetMaxResults() int
	GetTotal() int
	GetIssues() []IIssue
}

type IJiraClient interface {
	GetIssue(issueID string) (IIssue, error)
	GetIssuesCreated(product IProduct, startDate time.Time, endDate time.Time) ([]IIssue, error)
	SearchIssues(jql string, startAt, maxResults int) (IJiraSearchResult, error)
}
