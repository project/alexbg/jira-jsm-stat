package jiraclient

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

const (
	V2 = "rest/api/2"

	SearchQueryDateLayout     = "2006-01-02"
	SearchQueryDateTimeLayout = "2006-01-02 15:04"
)

type JiraClientV2 struct {
	Config IJiraClientConfig
}

func NewClientV2(cfg IJiraClientConfig) IJiraClient {
	return &JiraClientV2{Config: cfg}
}

func (c *JiraClientV2) api() string {
	cfg := c.Config
	log := cfg.GetLogger()

	apiURL, err := url.JoinPath(cfg.GetBaseUrl(), V2)
	if err != nil {
		log.Fatal(err.Error())
		return ""
	}

	return apiURL
}
func (c *JiraClientV2) withBearerToken(req *http.Request) {
	req.Header.Add("Authorization", "Bearer "+c.Config.GetApiToken())
}
func (c *JiraClientV2) withJsonContentType(req *http.Request) {
	req.Header.Add("Content-Type", "application/json")
}
func handlePossibleError(response *http.Response) error {
	if response.StatusCode == 200 {
		return nil
	}

	body, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	if err != nil {
		return err
	}

	return errors.New(string(body))
}
func (c *JiraClientV2) logRequest(name string, params ...any) {
	c.Config.GetLogger().Message("Jira-Client-V2: %s(%v)", name, params)
}

func (c *JiraClientV2) GetIssue(issueID string) (IIssue, error) {
	c.logRequest("GetIssue", issueID)
	requestURL, err := url.JoinPath(c.api(), "issue", issueID)
	if err != nil {
		return nil, err
	}

	requestURL += "?expand=changelog"
	req, err := http.NewRequest(http.MethodGet, requestURL, nil)
	if err != nil {
		return nil, err
	}

	c.withBearerToken(req)
	c.withJsonContentType(req)

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	err = handlePossibleError(response)
	if err != nil {
		return nil, err
	}

	issue := &JiraIssue{}
	err = json.NewDecoder(response.Body).Decode(issue)
	defer response.Body.Close()

	if err != nil {
		return nil, err
	}

	return issue, nil
}

func (c *JiraClientV2) SearchIssues(jql string, startAt, maxResults int) (IJiraSearchResult, error) {
	c.logRequest("SearchIssues", jql, startAt, maxResults)
	requestURL, err := url.JoinPath(c.api(), "search")
	if err != nil {
		return nil, err
	}

	requestURL += fmt.Sprintf("?jql=%s&startAt=%v&maxResults=%v",
		url.QueryEscape(jql),
		startAt, maxResults,
	)
	req, err := http.NewRequest(http.MethodGet, requestURL, nil)
	if err != nil {
		return nil, err
	}

	c.withBearerToken(req)
	c.withJsonContentType(req)

	response, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	err = handlePossibleError(response)
	if err != nil {
		return nil, err
	}

	result := &JiraSearchResult{}
	err = json.NewDecoder(response.Body).Decode(&result)
	defer response.Body.Close()
	if err != nil {
		return nil, err
	}

	return result, nil
}

func asStringList(list []string) string {
	return "\"" + strings.Join(list, "\", \"") + "\""
}
func getSearchQueryNoLabels(product IProduct, startDate time.Time, endDate time.Time) string {
	searchQuery := fmt.Sprintf(`project="%s" AND created >="%s" AND created <= "%s"`,
		product.ProjectID(),
		startDate.Format(SearchQueryDateTimeLayout),
		endDate.Format(SearchQueryDateTimeLayout),
	)
	return searchQuery
}
func getSearchQueryWithLabels(product IProduct, startDate time.Time, endDate time.Time) string {
	searchQuery := fmt.Sprintf(`project="%s" AND labels in (%s) AND created >="%s" AND created <= "%s"`,
		product.ProjectID(),
		asStringList(product.Labels()),
		startDate.Format(SearchQueryDateTimeLayout),
		endDate.Format(SearchQueryDateTimeLayout),
	)
	return searchQuery
}
func getSearchQuery(product IProduct, startDate time.Time, endDate time.Time) string {
	labels := product.Labels()
	if labels == nil || len(labels) == 0 {
		return getSearchQueryNoLabels(product, startDate, endDate)
	}

	return getSearchQueryWithLabels(product, startDate, endDate)
}

func (c *JiraClientV2) GetIssuesCreated(product IProduct, startDate time.Time, endDate time.Time) ([]IIssue, error) {
	searchQuery := getSearchQuery(product, startDate, endDate)
	const maxResults = 50
	startAt := 0
	result, err := c.SearchIssues(searchQuery, startAt, maxResults)
	if err != nil {
		return nil, err
	}

	issues := make([]IIssue, result.GetTotal())
	for {
		foundIssues := result.GetIssues()
		for i := 0; i < len(foundIssues); i++ {
			issues[startAt+i] = foundIssues[i]
		}

		startAt += maxResults
		if startAt >= result.GetTotal() {
			break
		}

		result, err = c.SearchIssues(searchQuery, startAt, maxResults)
		if err != nil {
			return nil, err
		}
	}

	return issues, nil
}
