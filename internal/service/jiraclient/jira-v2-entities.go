package jiraclient

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/mitchellh/mapstructure"
)

type JiraDateTime struct {
	time.Time
}

const DateTimeLayout = "2006-01-02T15:04:05"

func parseDateTime(dateTimeStr string) (time.Time, error) {
	dateTimeStr = strings.Trim(dateTimeStr, "\"")
	dotPos := strings.Index(dateTimeStr, ".")
	if dotPos > 0 {
		dateTimeStr = string(dateTimeStr[0:dotPos])
	}

	result, err := time.Parse(DateTimeLayout, dateTimeStr)
	return result, err
}
func (ct *JiraDateTime) UnmarshalJSON(b []byte) (err error) {
	dateTimeStr := string(b)
	ct.Time, err = parseDateTime(dateTimeStr)
	return
}

type JiraIssue struct {
	ID        string        `json:"id"`
	Key       string        `json:"key"`
	Fields    JiraFields    `json:"fields"`
	Changelog JiraChangelog `json:"changelog"`

	creator     IAuthor
	createdDate time.Time
	issueType   IIssueType
	labels      []string
	status      IIssueStatus
}

type JiraFields struct {
	Map map[string]interface{}
}

func (issue *JiraIssue) UnmarshalJSON(b []byte) error {
	type TempTypeAlias JiraIssue
	result := TempTypeAlias{}
	err := json.Unmarshal(b, &result)
	if err != nil {
		return err
	}

	*issue = JiraIssue(result)
	issue.issueType, err = issue.parseIssueType()
	if err != nil {
		return err
	}

	issue.createdDate, err = issue.parseCreatedDate()
	if err != nil {
		return err
	}

	issue.creator, err = issue.parseCreator()
	if err != nil {
		return err
	}

	issue.labels, err = issue.parseLabels()
	if err != nil {
		return err
	}

	issue.status, err = issue.parseStatus()
	if err != nil {
		return err
	}

	return nil
}

func (jf *JiraFields) UnmarshalJSON(b []byte) error {
	jf.Map = make(map[string]interface{})
	err := json.Unmarshal(b, &jf.Map)
	if err != nil {
		return err
	}

	return nil
}
func (issue *JiraIssue) errorKeyDoesntExist(name string) error {
	return errors.New(fmt.Sprintf("Issue %s: Key '%s' doesn't exist", issue.Key, name))
}
func (issue *JiraIssue) errorCantParse(name string) error {
	return errors.New(fmt.Sprintf("Issue %s: Can't parse '%s' value", issue.Key, name))
}
func (issue *JiraIssue) parseCreatedDate() (time.Time, error) {
	const key = "created"
	value, ok := issue.Fields.Map[key]
	if !ok {
		return time.Time{}, nil
	}

	createdDateStr, ok := value.(string)
	if !ok {
		return time.Time{}, issue.errorCantParse(key)
	}

	result, err := parseDateTime(createdDateStr)
	return result, err
}
func (issue *JiraIssue) parseIssueType() (IIssueType, error) {
	const key = "issuetype"
	value, ok := issue.Fields.Map[key]
	if !ok {
		return nil, nil
	}

	issueTypeMap, ok := value.(map[string]interface{})
	if !ok {
		return nil, issue.errorCantParse(key)
	}

	issueType := &JiraIssueType{}
	err := mapstructure.Decode(issueTypeMap, issueType)
	return issueType, err
}
func (issue *JiraIssue) parseCreator() (IAuthor, error) {
	const key = "creator"
	value, ok := issue.Fields.Map[key]
	if !ok {
		return nil, nil
	}

	creatorMap, ok := value.(map[string]interface{})
	if !ok {
		return nil, nil // don't throw error here because creator can be null for Issues migrated from another Jira (creator can't be found)
	}

	creator := &JiraAuthor{}
	err := mapstructure.Decode(creatorMap, creator)
	return creator, err
}
func (issue *JiraIssue) parseLabels() ([]string, error) {
	const key = "labels"
	value, ok := issue.Fields.Map[key]
	if !ok {
		return nil, nil
	}

	if value == nil {
		return nil, nil
	}

	labelsArray, ok := value.([]interface{})
	if !ok || labelsArray == nil || len(labelsArray) == 0 {
		return nil, nil
	}

	result := make([]string, len(labelsArray))
	for i, v := range labelsArray {
		strLabel, ok := v.(string)
		if !ok {
			return nil, issue.errorCantParse(key)
		}
		result[i] = strLabel
	}
	return result, nil
}
func (issue *JiraIssue) parseStatus() (IIssueStatus, error) {
	const key = "status"
	value, ok := issue.Fields.Map[key]
	if !ok {
		return nil, nil
	}

	statusMap, ok := value.(map[string]interface{})
	if !ok {
		return nil, issue.errorCantParse(key)
	}

	status := &JiraStatus{}
	err := mapstructure.Decode(statusMap, status)
	return status, err
}
func (issue *JiraIssue) IssueID() string {
	return issue.Key
}
func (issue *JiraIssue) Type() IIssueType {
	return issue.issueType
}
func (issue *JiraIssue) Creator() IAuthor {
	return issue.creator
}
func (issue *JiraIssue) Labels() []string {
	return issue.labels
}
func (issue *JiraIssue) Status() IIssueStatus {
	return issue.status
}
func (issue *JiraIssue) CreatedDate() time.Time {
	return issue.createdDate
}

func (issue *JiraIssue) HistoryItems() []IIssueHistoryItem {
	count := issue.Changelog.GetHistoryItemsCount()
	result := make([]IIssueHistoryItem, count)

	index := 0
	for i := 0; i < len(issue.Changelog.Histories); i++ {
		history := &issue.Changelog.Histories[i]
		for j := 0; j < len(history.Items); j++ {
			item := &history.Items[j]
			item.history = history
			result[index] = item
			index++
		}
	}
	return result
}

type JiraChangelog struct {
	Histories []JiraHistory `json:"histories"`
}

func (c *JiraChangelog) GetHistoryItemsCount() int {
	count := 0
	for _, history := range c.Histories {
		if history.Items != nil {
			count += len(history.Items)
		}
	}
	return count
}

type JiraHistory struct {
	ID      string            `json:"id"`
	Author  JiraAuthor        `json:"author"`
	Created JiraDateTime      `json:"created"`
	Items   []JiraHistoryItem `json:"items"`
}

type JiraAuthor struct {
	Name        string `json:"name"`
	DisplayName string `json:"displayName"`
}

func (author *JiraAuthor) ShortName() string {
	return author.Name
}
func (author *JiraAuthor) FullName() string {
	return author.DisplayName
}

type JiraHistoryItem struct {
	history *JiraHistory

	FieldName string `json:"field"`
	From      string `json:"from"`
	To        string `json:"to"`
	FromStr   string `json:"fromString"`
	ToStr     string `json:"toString"`
}

func (item *JiraHistoryItem) Author() IAuthor {
	return &item.history.Author
}
func (item *JiraHistoryItem) CreatedDate() time.Time {
	return item.history.Created.Time
}
func (item *JiraHistoryItem) Field() string {
	return item.FieldName
}
func (item *JiraHistoryItem) OldValue() string {
	return item.From
}
func (item *JiraHistoryItem) OldString() string {
	return item.FromStr
}
func (item *JiraHistoryItem) NewValue() string {
	return item.To
}
func (item *JiraHistoryItem) NewString() string {
	return item.ToStr
}

type JiraIssueType struct {
	ID   string
	Name string
}

func (t *JiraIssueType) TypeID() int {
	result, err := strconv.Atoi(t.ID)
	if err != nil {
		return -1
	}

	return result
}
func (t *JiraIssueType) DisplayName() string { return t.Name }

type JiraStatus struct {
	ID   string
	Name string
}

func (t *JiraStatus) StatusID() int {
	result, err := strconv.Atoi(t.ID)
	if err != nil {
		return -1
	}

	return result
}
func (t *JiraStatus) DisplayName() string { return t.Name }

type JiraSearchResult struct {
	StartAt    int         `json:"startAt"`
	MaxResults int         `json:"maxResults"`
	Total      int         `json:"total"`
	Issues     []JiraIssue `json:"issues"`
}

func (r *JiraSearchResult) GetStartAt() int {
	return r.StartAt
}
func (r *JiraSearchResult) GetMaxResults() int {
	return r.MaxResults
}
func (r *JiraSearchResult) GetTotal() int {
	return r.Total
}
func (r *JiraSearchResult) GetIssues() []IIssue {
	result := make([]IIssue, len(r.Issues))
	for i := 0; i < len(result); i++ {
		result[i] = &r.Issues[i]
	}
	return result
}
