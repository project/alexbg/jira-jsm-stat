package jiraclient

import (
	"fmt"
	"os"
	"testing"
	"time"

	"jsmstat/internal/config"
	"jsmstat/internal/service/jiraclient"

	"github.com/stretchr/testify/assert"
)

func AssertHistoryItem(t *testing.T, item jiraclient.IIssueHistoryItem, authorName, created, field, old, new string) {
	assert.Equal(t, authorName, item.Author().ShortName())
	assert.Equal(t, created, item.CreatedDate().Format(jiraclient.DateTimeLayout))
	assert.Equal(t, field, item.Field())
	assert.Equal(t, old, item.OldString())
	assert.Equal(t, new, item.NewString())
}

func TestIntegration_GetIssue_200(t *testing.T) {
	cfg := &MockJiraClientConfig{}
	cfg.On("GetBaseUrl").Return(os.Getenv(config.Env_BaseUrl)).
		On("GetApiToken").Return(os.Getenv(config.Env_ApiToken))

	client := jiraclient.NewClientV2(cfg)
	issue, err := client.GetIssue("EDO-10448")

	assert.NoError(t, err)
	assert.Equal(t, "EDO-10448", issue.IssueID())

	history := issue.HistoryItems()
	assert.Equal(t, 10, len(history))
	AssertHistoryItem(t, history[0], "shaneva_dm", "2023-04-28T13:43:39", "Comment", "", "")
	AssertHistoryItem(t, history[1], "shaneva_dm", "2023-04-28T14:13:09", "status", "Open", "Валидация")
	AssertHistoryItem(t, history[2], "shaneva_dm", "2023-04-28T14:13:10", "status", "Валидация", "Готово к разработке")
	AssertHistoryItem(t, history[3], "shaneva_dm", "2023-04-28T14:13:12", "status", "Готово к разработке", "Сделать")
	AssertHistoryItem(t, history[4], "astral-bot", "2023-04-28T14:13:12", "Commitment Point", "", "28.04.2023 17:13")
	AssertHistoryItem(t, history[5], "shaneva_dm", "2023-04-28T14:13:18", "assignee", "", "Шанёва Дарья Михайловна")
	AssertHistoryItem(t, history[6], "shaneva_dm", "2023-04-28T14:13:19", "status", "Сделать", "Выполняется")
	AssertHistoryItem(t, history[7], "shaneva_dm", "2023-04-28T14:14:33", "status", "Выполняется", "Приемка")
	AssertHistoryItem(t, history[8], "rodionov_ad", "2023-04-28T14:20:04", "resolution", "", "Готово")
	AssertHistoryItem(t, history[9], "rodionov_ad", "2023-04-28T14:20:04", "status", "Приемка", "Готово")

	createdDate := issue.CreatedDate()
	assert.Equal(t, "2023-04-28T13:15:10", createdDate.Format(jiraclient.DateTimeLayout))

	issueType := issue.Type()
	assert.NotNil(t, issueType)
	assert.Equal(t, 10009, issueType.TypeID())
	assert.Equal(t, "Поддержка", issueType.DisplayName())

	creator := issue.Creator()
	assert.NotNil(t, creator)
	assert.Equal(t, "rodionov_ad", creator.ShortName())
	assert.Equal(t, "Родионов Александр Дмитриевич", creator.FullName())

	labels := issue.Labels()
	assert.NotNil(t, labels)
	assert.Equal(t, []string{"run", "Оператор.ЭДО"}, labels)

	issueStatus := issue.Status()
	assert.NotNil(t, issueStatus)
	assert.Equal(t, 10030, issueStatus.StatusID())
	assert.Equal(t, "Готово", issueStatus.DisplayName())
}
func TestIntegration_GetIssue_404(t *testing.T) {
	cfg := &MockJiraClientConfig{}
	cfg.On("GetBaseUrl").Return(os.Getenv(config.Env_BaseUrl)).
		On("GetApiToken").Return(os.Getenv(config.Env_ApiToken))

	client := jiraclient.NewClientV2(cfg)
	issue, err := client.GetIssue("something")
	assert.Error(t, err)
	assert.Nil(t, issue)
}

func TestIntegration_SearchIssues_200(t *testing.T) {
	cfg := &MockJiraClientConfig{}
	cfg.On("GetBaseUrl").Return(os.Getenv(config.Env_BaseUrl)).
		On("GetApiToken").Return(os.Getenv(config.Env_ApiToken))

	client := jiraclient.NewClientV2(cfg)
	result, err := client.SearchIssues(`project="EDO"`, 0, 50)

	assert.NoError(t, err)
	assert.Equal(t, 0, result.GetStartAt())
	assert.Equal(t, 50, result.GetMaxResults())
	assert.Equal(t, 50, len(result.GetIssues()))

	m := make(map[string]jiraclient.IIssue)
	for _, issue := range result.GetIssues() {
		m[issue.IssueID()] = issue
	}

	result, err = client.SearchIssues(`project="EDO"`, 50, 100)
	assert.NoError(t, err)
	assert.Equal(t, 50, result.GetStartAt())
	assert.Equal(t, 100, result.GetMaxResults())
	assert.Equal(t, 100, len(result.GetIssues()))
	for _, issue := range result.GetIssues() {
		m[issue.IssueID()] = issue
	}

	assert.Equal(t, 150, len(m))
}
func TestIntegration_SearchIssues_400(t *testing.T) {
	cfg := &MockJiraClientConfig{}
	cfg.On("GetBaseUrl").Return(os.Getenv(config.Env_BaseUrl)).
		On("GetApiToken").Return(os.Getenv(config.Env_ApiToken))

	client := jiraclient.NewClientV2(cfg)
	result, err := client.SearchIssues(`project="EDO" AND xyz="xyz"`, 0, 50)
	assert.Error(t, err)
	assert.Nil(t, result)
}

const DateLayout = "2006-01-02"

func TestIntegration_GetIssuesCreated_200(t *testing.T) {
	cfg := &MockJiraClientConfig{}
	cfg.On("GetBaseUrl").Return(os.Getenv(config.Env_BaseUrl)).
		On("GetApiToken").Return(os.Getenv(config.Env_ApiToken))

	product := &MockProduct{}
	product.On("ProjectID").Return("EDO").
		On("Labels").Return([]string{"Оператор.ЭДО"})

	client := jiraclient.NewClientV2(cfg)
	startDate, _ := time.Parse(DateLayout, "2022-03-01")
	endDate, _ := time.Parse(DateLayout, "2022-04-01")
	result, err := client.GetIssuesCreated(product, startDate, endDate)

	assert.NoError(t, err)
	assert.NotNil(t, result)
	assert.Equal(t, 97, len(result))

	for i, issue := range result {
		assert.Contains(t, issue.Labels(), "Оператор.ЭДО", fmt.Sprintf("Element Index = %v", i))
	}
}
func TestIntegration_GetIssuesCreated_NoLabels_200(t *testing.T) {
	cfg := &MockJiraClientConfig{}
	cfg.On("GetBaseUrl").Return(os.Getenv(config.Env_BaseUrl)).
		On("GetApiToken").Return(os.Getenv(config.Env_ApiToken))

	product := &MockProduct{}
	product.On("ProjectID").Return("EDO").
		On("Labels").Return([]string{})

	client := jiraclient.NewClientV2(cfg)
	startDate, _ := time.Parse(DateLayout, "2022-03-01")
	endDate, _ := time.Parse(DateLayout, "2022-04-01")
	result, err := client.GetIssuesCreated(product, startDate, endDate)

	assert.NoError(t, err)
	assert.NotNil(t, result)
	assert.Equal(t, 298, len(result))
}
