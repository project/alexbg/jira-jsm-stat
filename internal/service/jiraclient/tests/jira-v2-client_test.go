package jiraclient

import (
	"encoding/json"
	"jsmstat/internal/service/jiraclient"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIssue_Empty(t *testing.T) {
	testJson := `{}`

	issue := &jiraclient.JiraIssue{}
	err := json.Unmarshal([]byte(testJson), issue)

	assert.NoError(t, err)
	assert.Equal(t, 0, issue.Changelog.GetHistoryItemsCount())

	items := issue.HistoryItems()
	assert.NotNil(t, items)
	assert.Equal(t, 0, len(items))
}
func TestIssue_Changelog(t *testing.T) {
	testJson := `{
		"changelog": {
			"startAt": 0,
			"maxResults": 2,
			"total": 2,
			"histories": [
				{
					"author": {
						"name": "shaneva_dm",
						"displayName": "Шанёва Дарья Михайловна"
					},
					"created": "2023-04-28T13:43:39.174+0000",
					"items": [
						{
							"field": "Comment",
							"from": "test",
							"fromString": null,
							"to": null,
							"toString": null
						}
					]
				},
				{
					"author": {
						"name": "rodionov_ad",
						"displayName": "Родионов Александр Дмитриевич"
					},
					"created": "2023-04-28T14:20:04.944+0000",
					"items": [
						{
							"field": "resolution",
							"fieldtype": "jira",
							"from": null,
							"fromString": null,
							"to": "10000",
							"toString": "Готово"
						},
						{
							"field": "status",
							"fieldtype": "jira",
							"from": "10095",
							"fromString": "Приемка",
							"to": "10030",
							"toString": "Готово"
						}
					]
				}
			]
		}
	}`

	issue := &jiraclient.JiraIssue{}
	err := json.Unmarshal([]byte(testJson), issue)

	assert.NoError(t, err)
	assert.Equal(t, 3, issue.Changelog.GetHistoryItemsCount())

	items := issue.HistoryItems()
	assert.NotNil(t, items)
	assert.Equal(t, 3, len(items))

	AssertHistoryItem(t, items[0], "shaneva_dm", "2023-04-28T13:43:39", "Comment", "", "")
	assert.Equal(t, "test", items[0].OldValue())

	AssertHistoryItem(t, items[1], "rodionov_ad", "2023-04-28T14:20:04", "resolution", "", "Готово")
	AssertHistoryItem(t, items[2], "rodionov_ad", "2023-04-28T14:20:04", "status", "Приемка", "Готово")
}
func TestIssue_Created(t *testing.T) {
	testJson := `{
		"fields": {
			"created": "2023-04-28T13:15:10.280+0000"
		}
	}`

	issue := &jiraclient.JiraIssue{}
	err := json.Unmarshal([]byte(testJson), issue)
	assert.NoError(t, err)

	createdTime := issue.CreatedDate()
	assert.Equal(t, "2023-04-28T13:15:10", createdTime.Format(jiraclient.DateTimeLayout))
}
func TestIssue_Type(t *testing.T) {
	testJson := `{
		"fields": {
			"issuetype": {
				"id": "10009",
				"name": "Поддержка"
			}
		}
	}`

	issue := &jiraclient.JiraIssue{}
	err := json.Unmarshal([]byte(testJson), issue)
	assert.NoError(t, err)

	issueType := issue.Type()
	assert.NotNil(t, issueType)
	assert.Equal(t, 10009, issueType.TypeID())
	assert.Equal(t, "Поддержка", issueType.DisplayName())
}
func TestIssueType_NoTypeID(t *testing.T) {
	issueType := &jiraclient.JiraIssueType{}
	assert.Equal(t, -1, issueType.TypeID())
}
func TestIssueType_TypeIDHasInvalidFormat(t *testing.T) {
	issueType := &jiraclient.JiraIssueType{
		ID: "bla bla",
	}
	assert.Equal(t, -1, issueType.TypeID())
}
func TestIssue_Labels(t *testing.T) {
	testJson := `{
		"fields": {
			"labels": [
				"run",
				"Оператор.ЭДО"
			]
		}
	}`

	issue := &jiraclient.JiraIssue{}
	err := json.Unmarshal([]byte(testJson), issue)
	assert.NoError(t, err)

	labels := issue.Labels()
	assert.NotNil(t, labels)
	assert.Equal(t, []string{"run", "Оператор.ЭДО"}, labels)
}
func TestIssue_Status(t *testing.T) {
	testJson := `{
		"fields": {
			"status": {
				"name": "Готово",
				"id": "10030"
			}
		}
	}`

	issue := &jiraclient.JiraIssue{}
	err := json.Unmarshal([]byte(testJson), issue)
	assert.NoError(t, err)

	status := issue.Status()
	assert.NotNil(t, status)
	assert.Equal(t, 10030, status.StatusID())
	assert.Equal(t, "Готово", status.DisplayName())
}
func TestIssueStatus_NoStatusID(t *testing.T) {
	status := &jiraclient.JiraStatus{}
	assert.Equal(t, -1, status.StatusID())
}
func TestIssueStatus_StatusIDHasInvalidFormat(t *testing.T) {
	status := &jiraclient.JiraStatus{
		ID: "bla bla",
	}
	assert.Equal(t, -1, status.StatusID())
}
func TestIssue_Creator(t *testing.T) {
	testJson := `{
		"fields": {
			"creator": {
				"name": "rodionov_ad",
				"displayName": "Родионов Александр Дмитриевич",
				"active": true
			}
		}
	}`

	issue := &jiraclient.JiraIssue{}
	err := json.Unmarshal([]byte(testJson), issue)
	assert.NoError(t, err)

	creator := issue.Creator()
	assert.NotNil(t, creator)
	assert.Equal(t, "rodionov_ad", creator.ShortName())
	assert.Equal(t, "Родионов Александр Дмитриевич", creator.FullName())
}
