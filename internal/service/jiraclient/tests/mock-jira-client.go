package jiraclient

import (
	"jsmstat/internal/service/jiraclient"
	"jsmstat/pkg/logger"
	"time"

	"github.com/stretchr/testify/mock"
)

type MockJiraClientConfig struct {
	mock.Mock
}

func (m *MockJiraClientConfig) GetLogger() logger.ILogger {
	return &logger.DefaultLogger{}
}
func (m *MockJiraClientConfig) GetBaseUrl() string {
	args := m.Called()
	return args.String(0)
}
func (m *MockJiraClientConfig) GetApiToken() string {
	args := m.Called()
	return args.String(0)
}

type MockProduct struct {
	mock.Mock
}

func (m *MockProduct) ProjectID() string {
	args := m.Called()
	return args.String(0)
}
func (m *MockProduct) Labels() []string {
	args := m.Called()
	return args.Get(0).([]string)
}

type MockJiraClient struct {
	mock.Mock
}

func (m *MockJiraClient) GetIssue(issueID string) (jiraclient.IIssue, error) {
	args := m.Called(issueID)
	return args.Get(0).(jiraclient.IIssue), args.Error(1)
}
func (m *MockJiraClient) GetIssuesCreated(product jiraclient.IProduct, startDate time.Time, endDate time.Time) ([]jiraclient.IIssue, error) {
	args := m.Called(product, startDate, endDate)
	return args.Get(0).([]jiraclient.IIssue), args.Error(1)
}
func (m *MockJiraClient) SearchIssues(jql string, startAt, maxResults int) (jiraclient.IJiraSearchResult, error) {
	args := m.Called(jql, startAt, maxResults)
	return args.Get(0).(jiraclient.IJiraSearchResult), args.Error(1)
}
