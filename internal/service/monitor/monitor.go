package monitor

import (
	"errors"
	"fmt"
	"sort"
	"strconv"
	"time"

	"jsmstat/internal/domain"
	"jsmstat/internal/repositories"
	"jsmstat/internal/service/jiraclient"
	"jsmstat/pkg/errx"
	"jsmstat/pkg/flags"
	"jsmstat/pkg/logger"
)

type IMonitorConfig interface {
	jiraclient.IProduct

	GetLogger() logger.ILogger
	GetProductName() string
	GetMonitoringDuration() time.Duration
	GetMonitoringStartDate() time.Time
}

type IMonitor interface {
	Start()
	Stop()
}

type DefaultMonitor struct {
	Config          IMonitorConfig
	TaskHistoryRepo repositories.ITaskHistoryRepository
	JiraClient      jiraclient.IJiraClient

	ticker   *time.Ticker
	done     chan bool
	lastTime time.Time
}

func InitMonitor(cfg IMonitorConfig, jiraClient jiraclient.IJiraClient, repo repositories.ITaskHistoryRepository) IMonitor {
	return &DefaultMonitor{
		Config:          cfg,
		TaskHistoryRepo: repo,
		JiraClient:      jiraClient,
	}
}

func (m *DefaultMonitor) errorWithPrefix(str string) string {
	return fmt.Sprintf("Jira-Monitor (%s) Error: %v", m.Config.GetProductName(), str)
}
func (m *DefaultMonitor) withPrefix(str string) string {
	return fmt.Sprintf("Jira-Monitor (%s): %v", m.Config.GetProductName(), str)
}
func (m *DefaultMonitor) Start() {
	log := m.Config.GetLogger()

	if m.ticker != nil || m.done != nil {
		log.Fatal(m.errorWithPrefix("Can't run another Worker. Worker has already been run"))
		return
	}

	m.ticker = time.NewTicker(m.Config.GetMonitoringDuration())
	m.done = make(chan bool)

	go m.worker()
}
func (m *DefaultMonitor) Stop() {
	m.ticker.Stop()
	m.done <- true
}
func (m *DefaultMonitor) worker() {
	cfg := m.Config
	log := cfg.GetLogger()

	log.Message(m.withPrefix(
		fmt.Sprintf("Duration = %v, StartDate = %v",
			cfg.GetMonitoringDuration(),
			cfg.GetMonitoringStartDate().Format(flags.DateFormat),
		)))

	log.Message(m.withPrefix("Worker started..."))
	defer func() {
		log.Message(m.withPrefix("Worker stopped"))
	}()

	for {
		select {
		case <-m.ticker.C:
			log.Message(m.withPrefix("Checking updates..."))
			err := m.SaveHistoryItemsForCreatedIssues()
			if err != nil {
				log.Message(err.Error())
			}
		case <-m.done:
			return
		}
	}
}
func (m *DefaultMonitor) GetStartTimeStamp() (time.Time, error) {
	cfgStartTime := m.Config.GetMonitoringStartDate()
	dbStartTime, err := m.TaskHistoryRepo.GetLastTimeStamp(domain.Created)

	if err != nil {
		return time.Time{}, err
	}

	timeSlice := []time.Time{
		cfgStartTime, dbStartTime, m.lastTime,
	}

	sort.Slice(timeSlice, func(i, j int) bool {
		return timeSlice[i].After(timeSlice[j])
	})

	return timeSlice[0], nil
}
func (m *DefaultMonitor) SetLastTimeStamp(newLastTime time.Time) {
	m.lastTime = newLastTime
}

func convertToCreationItem(issue jiraclient.IIssue) *domain.TaskHistoryItem {
	authorShortName := ""
	creator := issue.Creator()
	if creator != nil { // creator can be null for issues migrated from Jira For Cloud
		authorShortName = creator.ShortName()
	}

	item := &domain.TaskHistoryItem{
		TimeStamp:   issue.CreatedDate(),
		IssueID:     issue.IssueID(),
		IssueTypeID: issue.Type().TypeID(),
		IssueType:   issue.Type().DisplayName(),
		Author:      authorShortName,
		StatusID:    issue.Status().StatusID(),
		StatusText:  issue.Status().DisplayName(),
		Operation:   domain.Created,
	}
	return item
}
func convertToChangeStatusItem(issue jiraclient.IIssue, item jiraclient.IIssueHistoryItem) (*domain.TaskHistoryItem, error) {
	if item.Field() != "status" {
		return nil, errors.New(fmt.Sprintf("%s: Issue History Item %v can't be converted to ChangeStatusItem", issue.IssueID(), item))
	}

	statusID, err := strconv.Atoi(item.NewValue())
	if err != nil {
		return nil, errx.Wrap(fmt.Sprintf("%s: Issue History Item %v.", issue.IssueID(), item), err)
	}

	result := &domain.TaskHistoryItem{
		TimeStamp:   item.CreatedDate(),
		IssueID:     issue.IssueID(),
		IssueTypeID: issue.Type().TypeID(),
		IssueType:   issue.Type().DisplayName(),
		Author:      item.Author().ShortName(),
		StatusID:    statusID,
		StatusText:  item.NewString(),
		Operation:   domain.Updated,
	}
	return result, nil
}
func addStatusChangedHistoryItems(creationItem *domain.TaskHistoryItem, issue jiraclient.IIssue) ([]*domain.TaskHistoryItem, error) {
	items := issue.HistoryItems()
	result := make([]*domain.TaskHistoryItem, 0, len(items)+1)
	result = append(result, creationItem)

	for _, item := range items {
		if item.Field() != "status" {
			continue
		}

		historyItem, err := convertToChangeStatusItem(issue, item)
		if err != nil {
			return nil, err
		}
		result = append(result, historyItem)
	}
	return result, nil
}
func (m *DefaultMonitor) SaveHistoryItemsForCreatedIssues() error {
	startTime, err := m.GetStartTimeStamp()
	if err != nil {
		return err
	}

	endTime := startTime.AddDate(0, 1, 0)
	issues, err := m.JiraClient.GetIssuesCreated(m.Config, startTime, endTime)
	if err != nil {
		return err
	}

	for _, issue := range issues {
		creationItem := convertToCreationItem(issue)
		issueWithChangelog, err := m.JiraClient.GetIssue(issue.IssueID())
		if err != nil {
			return err
		}

		if issue.Creator() == nil && issueWithChangelog.Creator() != nil {
			creationItem.Author = issueWithChangelog.Creator().ShortName() // fix for issues migrated from Jira for Cloud
		}

		allHistoryItems, err := addStatusChangedHistoryItems(creationItem, issueWithChangelog)
		if err != nil {
			return err
		}

		err = m.TaskHistoryRepo.SaveItems(allHistoryItems)
		if err != nil {
			return err
		}
	}

	m.SetLastTimeStamp(endTime)
	return nil
}
