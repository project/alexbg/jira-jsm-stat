package monitor

import (
	"jsmstat/pkg/logger"
	"time"

	"github.com/stretchr/testify/mock"
)

type MockMonitorConfig struct {
	mock.Mock
}

const DateFormat = "2006-01-02"

func (cfg *MockMonitorConfig) ProjectID() string {
	args := cfg.Called()
	return args.String(0)
}
func (cfg *MockMonitorConfig) Labels() []string {
	args := cfg.Called()
	return args.Get(0).([]string)
}
func (cfg *MockMonitorConfig) GetLogger() logger.ILogger {
	args := cfg.Called()
	return args.Get(0).(logger.ILogger)
}
func (cfg *MockMonitorConfig) GetProductName() string {
	args := cfg.Called()
	return args.String(0)
}
func (cfg *MockMonitorConfig) GetMonitoringDuration() time.Duration {
	args := cfg.Called()
	return args.Get(0).(time.Duration)
}
func (cfg *MockMonitorConfig) GetMonitoringStartDate() time.Time {
	args := cfg.Called()
	result, _ := time.Parse(DateFormat, args.String(0))
	return result
}
