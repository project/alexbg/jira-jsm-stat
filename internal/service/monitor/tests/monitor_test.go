package monitor

import (
	"errors"
	"testing"
	"time"

	repotest "jsmstat/internal/repositories/tests"
	jctest "jsmstat/internal/service/jiraclient/tests"
	"jsmstat/internal/service/monitor"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func Test_GetStartTimeStamp_Error(t *testing.T) {
	cfg := &MockMonitorConfig{}
	cfg.On("GetMonitoringStartDate").Return("2022-01-01")

	client := &jctest.MockJiraClient{}
	repo := &repotest.MockTaskHistoryRepository{}
	repo.On("GetLastTimeStamp", mock.Anything).Return(nil, errors.New("fail"))

	m := monitor.InitMonitor(cfg, client, repo).(*monitor.DefaultMonitor)

	startTime, err := m.GetStartTimeStamp()
	assert.Error(t, err)
	assert.Empty(t, startTime)
}
func Test_GetStartTimeStamp_DBLastTimeIsZero(t *testing.T) {
	const cfgTestTime = "2022-01-01"
	cfg := &MockMonitorConfig{}
	cfg.On("GetMonitoringStartDate").Return(cfgTestTime)

	client := &jctest.MockJiraClient{}
	repo := &repotest.MockTaskHistoryRepository{}
	repo.On("GetLastTimeStamp", mock.Anything).Return(time.Time{}, nil)

	m := monitor.InitMonitor(cfg, client, repo).(*monitor.DefaultMonitor)

	startTime, err := m.GetStartTimeStamp()
	assert.NoError(t, err)
	assert.Equal(t, cfgTestTime, startTime.Format(DateFormat))
}
func Test_GetStartTimeStamp_DBLastTimeBeforeCfg(t *testing.T) {
	const cfgTestTime = "2022-01-01"
	const dbTestTime = "2021-12-01"
	cfg := &MockMonitorConfig{}
	cfg.On("GetMonitoringStartDate").Return(cfgTestTime)

	client := &jctest.MockJiraClient{}
	repo := &repotest.MockTaskHistoryRepository{}
	repo.On("GetLastTimeStamp", mock.Anything).Return(dbTestTime, nil)

	m := monitor.InitMonitor(cfg, client, repo).(*monitor.DefaultMonitor)

	startTime, err := m.GetStartTimeStamp()
	assert.NoError(t, err)
	assert.Equal(t, cfgTestTime, startTime.Format(DateFormat))
}
func Test_GetStartTimeStamp_DBLastTimeAfterCfg(t *testing.T) {
	const cfgTestTime = "2022-01-01"
	const dbTestTime = "2022-03-01"
	cfg := &MockMonitorConfig{}
	cfg.On("GetMonitoringStartDate").Return(cfgTestTime)

	client := &jctest.MockJiraClient{}
	repo := &repotest.MockTaskHistoryRepository{}
	repo.On("GetLastTimeStamp", mock.Anything).Return(dbTestTime, nil)

	m := monitor.InitMonitor(cfg, client, repo).(*monitor.DefaultMonitor)

	startTime, err := m.GetStartTimeStamp()
	assert.NoError(t, err)
	assert.Equal(t, dbTestTime, startTime.Format(DateFormat))
}
func Test_GetStartTimeStamp_SetLastTime(t *testing.T) {
	const cfgTestTime = "2022-01-01"
	const dbTestTime = "2022-03-01"
	const lastTime = "2022-04-01"
	cfg := &MockMonitorConfig{}
	cfg.On("GetMonitoringStartDate").Return(cfgTestTime)

	client := &jctest.MockJiraClient{}
	repo := &repotest.MockTaskHistoryRepository{}
	repo.On("GetLastTimeStamp", mock.Anything).Return(dbTestTime, nil)

	m := monitor.InitMonitor(cfg, client, repo).(*monitor.DefaultMonitor)
	lt, _ := time.Parse(DateFormat, lastTime)
	m.SetLastTimeStamp(lt)

	startTime, err := m.GetStartTimeStamp()
	assert.NoError(t, err)
	assert.Equal(t, lastTime, startTime.Format(DateFormat))
}
