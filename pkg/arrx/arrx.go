package arrx

func Contains[t comparable](array []t, element t) bool {
	for i := range array {
		if array[i] == element {
			return true
		}
	}
	return false
}
