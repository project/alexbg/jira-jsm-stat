package clickhouse

import (
	"database/sql"
	"fmt"
	"reflect"
	"strings"

	"jsmstat/pkg/arrx"
	"jsmstat/pkg/db"

	_ "github.com/ClickHouse/clickhouse-go"
	"github.com/fatih/structs"
	"github.com/jmoiron/sqlx"
)

type clickhouseHelper struct {
	cfg             db.IDBConfig
	db              *sqlx.DB
	typeConvertions map[string]func(interface{}) interface{}
}

func NewHelper(cfg db.IDBConfig) db.IDBHelper {
	return &clickhouseHelper{cfg: cfg}
}

func (h *clickhouseHelper) SetTypeConversion(t reflect.Type, convertion func(interface{}) interface{}) {
	if h.typeConvertions == nil {
		h.typeConvertions = make(map[string]func(interface{}) interface{})
	}
	h.typeConvertions[t.Name()] = convertion
}
func (h *clickhouseHelper) getConnString() string {
	return fmt.Sprintf(
		"tcp://%s:%v?username=%s&password=%s&database=%s",
		h.cfg.GetDBHost(),
		h.cfg.GetDBPort(),
		h.cfg.GetDBUser(),
		h.cfg.GetDBPassword(),
		h.cfg.GetDBName(),
	)
}
func (h *clickhouseHelper) Database() (db.ISqlDB, error) {
	config := h.cfg
	log := config.GetLogger()

	if h.db != nil {
		err := h.db.Ping()
		if err == nil {
			return h, nil
		}
	}

	log.Message("Connecting to Clickhouse: %v:%v ...", config.GetDBHost(), config.GetDBPort())

	connstring := h.getConnString()
	conn, err := sqlx.Connect("clickhouse", connstring)
	if err != nil {
		return nil, err
	}

	log.Message("Connecting to Clickhouse: %v:%v SUCCESS", config.GetDBHost(), config.GetDBPort())
	h.db = conn
	return h, nil
}
func (h *clickhouseHelper) IsTableExists(tableName string) (bool, error) {
	db, err := h.Database()
	if err != nil {
		return false, err
	}

	config := h.cfg
	sql := fmt.Sprintf("EXISTS TABLE %s.%s", config.GetDBName(), tableName)

	var result bool
	err = db.QueryRowAndScan(sql, &result)
	if err != nil {
		return false, err
	}

	return result, nil
}
func (h *clickhouseHelper) wrapInsertInTransaction(sql string, args ...any) error {
	db, err := h.Database()
	if err != nil {
		return err
	}

	tr, err := db.Begin()
	if err != nil {
		return err
	}
	defer tr.Rollback()

	if err = tr.ExecSql(sql, args...); err != nil {
		return err
	}

	return tr.Commit()
}
func (h *clickhouseHelper) InsertValues(tableName string, values []any, exceptFields []string) error {
	sql, fieldNames := buildInsertSql(tableName, values[0], exceptFields)
	for _, v := range values {
		args := h.getValuesFromFields(v, fieldNames)
		if err := h.wrapInsertInTransaction(sql, args...); err != nil {
			return err
		}
	}

	return nil
}
func buildInsertSql(tableName string, v any, exceptFields []string) (string, []string) {
	vmap := structs.Map(v)
	count := len(vmap) - len(exceptFields)
	fields := make([]string, 0, count)

	placeHolders := make([]string, count)
	for i := range placeHolders {
		placeHolders[i] = "?"
	}

	for key, _ := range vmap {
		if !arrx.Contains(exceptFields, key) {
			fields = append(fields, key)
		}
	}

	return fmt.Sprintf(
		"INSERT INTO %s (%s) VALUES (%s)",
		tableName,
		strings.Join(fields, ","),
		strings.Join(placeHolders, ","),
	), fields
}
func (h *clickhouseHelper) getValuesFromFields(v any, fields []string) []any {
	vtype := reflect.TypeOf(v)
	for vtype.Kind() == reflect.Ptr || vtype.Kind() == reflect.Interface {
		vtype = vtype.Elem()
	}

	vmap := structs.Map(v)
	count := len(fields)
	values := make([]any, 0, count)
	for _, fieldName := range fields {
		fieldValue := vmap[fieldName]
		field, ok := vtype.FieldByName(fieldName)
		if ok {
			if convertion, ok := h.typeConvertions[field.Type.Name()]; ok {
				fieldValue = convertion(fieldValue)
			}
		}

		values = append(values, fieldValue)
	}
	return values
}

// ISqlDB interface implementation
func (h *clickhouseHelper) QueryRowAndScan(sql string, dest ...any) error {
	stmt, err := h.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()

	err = stmt.QueryRow().Scan(dest...)
	return err
}
func (h *clickhouseHelper) QueryRows(sql string, process func(*sql.Rows) (bool, error), args ...any) error {
	stmt, err := h.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()

	rows, err := stmt.Query(args...)
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		if next, err := process(rows); !next || err != nil {
			return err
		}
	}

	return rows.Err()
}
func (h *clickhouseHelper) ExecSql(sql string, args ...any) error {
	stmt, err := h.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()

	if _, err = stmt.Exec(args...); err != nil {
		return err
	}

	return err
}
func (h *clickhouseHelper) Begin() (db.ISqlTransaction, error) {
	tx, err := h.db.Beginx()
	if err != nil {
		return nil, err
	}
	return &transaction{tx: tx}, nil
}
