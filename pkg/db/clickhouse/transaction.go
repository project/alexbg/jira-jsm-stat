package clickhouse

import "github.com/jmoiron/sqlx"

type transaction struct {
	tx *sqlx.Tx
}

func (t *transaction) Rollback() error {
	return t.tx.Rollback()
}
func (t *transaction) Commit() error {
	return t.tx.Commit()
}
func (t *transaction) QueryRowAndScan(sql string, dest ...any) error {
	stmt, err := t.tx.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()

	err = stmt.QueryRow().Scan(dest...)
	return err
}
func (t *transaction) ExecSql(sql string, args ...any) error {
	if _, err := t.tx.Exec(sql, args...); err != nil {
		return err
	}

	return nil
}
