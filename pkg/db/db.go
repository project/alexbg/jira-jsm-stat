package db

import (
	"database/sql"
	"jsmstat/pkg/logger"
	"reflect"
)

type IDBConfig interface {
	GetLogger() logger.ILogger
	GetDBHost() string
	GetDBPort() int
	GetDBName() string
	GetDBUser() string
	GetDBPassword() string
}

type ISqlDB interface {
	Begin() (ISqlTransaction, error)
	QueryRowAndScan(sql string, dest ...any) error
	QueryRows(sql string, process func(*sql.Rows) (bool, error), args ...any) error
	ExecSql(sql string, args ...any) error
}

type ISqlTransaction interface {
	Rollback() error
	Commit() error
	QueryRowAndScan(sql string, dest ...any) error
	ExecSql(sql string, args ...any) error
}

type IDBHelper interface {
	SetTypeConversion(t reflect.Type, convertion func(interface{}) interface{})
	Database() (ISqlDB, error)
	IsTableExists(tableName string) (bool, error)
	InsertValues(tableName string, values []interface{}, exceptFields []string) error
}
