package db

import (
	"database/sql"
	"reflect"

	"github.com/stretchr/testify/mock"
)

type MockDBHelper struct {
	mock.Mock
}

func (m *MockDBHelper) SetTypeConversion(t reflect.Type, convertion func(interface{}) interface{}) {
	//m.Called(t, convertion)
}
func (m *MockDBHelper) Database() (ISqlDB, error) {
	args := m.Called()
	return m, args.Error(0)
}
func (m *MockDBHelper) Begin() (ISqlTransaction, error) {
	args := m.Called()
	return args.Get(0).(ISqlTransaction), args.Error(0)
}
func (m *MockDBHelper) IsTableExists(tableName string) (bool, error) {
	args := m.Called(tableName)
	return args.Bool(0), args.Error(1)
}
func (m *MockDBHelper) InsertValues(tableName string, values []any, exceptFields []string) error {
	args := m.Called(tableName)
	return args.Error(0)
}
func (m *MockDBHelper) QueryRowAndScan(sql string, dest ...any) error {
	args := m.Called(sql)
	return args.Error(0)
}
func (m *MockDBHelper) QueryRows(sql string, process func(*sql.Rows) (bool, error), args ...any) error {
	return nil
}
func (m *MockDBHelper) ExecSql(sql string, any ...any) error {
	args := m.Called(sql)
	return args.Error(0)
}
