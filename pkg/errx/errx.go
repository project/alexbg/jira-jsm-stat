package errx

import (
	"errors"
)

func Wrap(message string, err error) error {
	if err != nil {
		return errors.New(message + "\n" + err.Error())
	}
	return errors.New(message)
}
