package flags

import (
	"flag"
	"fmt"
	"jsmstat/pkg/logger"
	"time"
)

type IFlagsProvider interface {
	StringVar(p *string, name string, value string, usage string)
	IntVar(p *int, name string, value int, usage string)
	DurationVar(p *time.Duration, name string, value time.Duration, usage string)
	DateVar(p *time.Time, name string, value time.Time, usage string)
	SetUsage(usage func())
	GetCommandLine() *flag.FlagSet
	PrintDefaults()
	Parse()
}

const DateFormat = "2006-01-02"

type backingVar struct {
	variable string
	parse    func(v *backingVar)
}
type DefaultFlagsProvider struct {
	logger      logger.ILogger
	backingVars map[string]backingVar
}

func NewDefaultFlagProvider(logger logger.ILogger) IFlagsProvider {
	return &DefaultFlagsProvider{
		logger:      logger,
		backingVars: make(map[string]backingVar),
	}
}

func (fp *DefaultFlagsProvider) StringVar(p *string, name string, value string, usage string) {
	flag.StringVar(p, name, value, usage)
}

func (fp *DefaultFlagsProvider) IntVar(p *int, name string, value int, usage string) {
	flag.IntVar(p, name, value, usage)
}

func (fp *DefaultFlagsProvider) DurationVar(p *time.Duration, name string, value time.Duration, usage string) {
	flag.DurationVar(p, name, value, usage)
}

func (fp *DefaultFlagsProvider) DateVar(p *time.Time, name string, value time.Time, usage string) {
	backingVar := backingVar{
		parse: func(v *backingVar) {
			t, err := time.Parse(DateFormat, v.variable)
			if err != nil {
				fp.logger.Fatal(fmt.Sprintf("Parsing '%v'. '%v' has invalid format. Use format like %v\n %v", name, v, DateFormat, err))
			}
			*p = t
		},
	}

	flag.StringVar(&backingVar.variable, name, value.Format(DateFormat), usage)
	fp.backingVars[name] = backingVar
}

func (fp *DefaultFlagsProvider) SetUsage(usage func()) {
	flag.Usage = usage
}

func (fp *DefaultFlagsProvider) GetCommandLine() *flag.FlagSet {
	return flag.CommandLine
}

func (fp *DefaultFlagsProvider) PrintDefaults() {
	flag.PrintDefaults()
}

func (fp *DefaultFlagsProvider) Parse() {
	flag.Parse()
	for key := range fp.backingVars {
		v := fp.backingVars[key]
		v.parse(&v)
	}
}
