package os

import (
	"io/fs"
	"os"
)

type IOsProvider interface {
	GetEnv(varName string) string
	Stat(filePath string) (fs.FileInfo, error)
	ReadFile(filePath string) ([]byte, error)
	GetArgs() []string
}

type DefaultOsProvider struct {
}

func (osp *DefaultOsProvider) GetEnv(varName string) string {
	return os.Getenv(varName)
}

func (osp *DefaultOsProvider) Stat(filePath string) (fs.FileInfo, error) {
	return os.Stat(filePath)
}

func (osp *DefaultOsProvider) ReadFile(filePath string) ([]byte, error) {
	return os.ReadFile(filePath)
}

func (osp *DefaultOsProvider) GetArgs() []string {
	return os.Args
}
